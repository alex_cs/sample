package com.cs.liker.bluetooth.test.ui.notsupport;


import android.view.View;

import com.cs.android_base.platform.BasePresenterView;
import com.cs.liker.bluetooth.test.R;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.Subscriptions;

public class NotSupportScreenView extends BasePresenterView {

    @Inject
    NotSupportScreenView() {
        super(R.layout.view_not_support);
    }

    @Override
    public void injectViews(View view) {

    }

    @Override
    public Subscription bindPresenter() {
        return Subscriptions.empty();
    }
}
