package com.cs.liker.bluetooth.test.analytics.injection;


import android.app.Application;

import com.cs.liker.bluetooth.test.analytics.domain.AnalyticTracker;
import com.cs.liker.bluetooth.test.analytics.platform.AnalyticTrackerGoogleImpl;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AnalyticTrackerModule {

    @Provides
    @Singleton
    AnalyticTracker provideAnalyticTracker(AnalyticTrackerGoogleImpl analyticTrackerGoogle) {
        return analyticTrackerGoogle;
    }

    @Provides
    @Singleton
    Tracker provideTracker(Application application) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(application);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        return analytics.newTracker("UA-85048749-1");
    }
}
