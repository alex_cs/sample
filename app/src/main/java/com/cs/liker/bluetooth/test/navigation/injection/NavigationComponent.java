package com.cs.liker.bluetooth.test.navigation.injection;


import com.cs.liker.bluetooth.test.MainActivity;

public interface NavigationComponent {
    void inject(MainActivity mainActivity);
}
