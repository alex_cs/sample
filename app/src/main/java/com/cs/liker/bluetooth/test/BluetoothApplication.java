package com.cs.liker.bluetooth.test;


import com.cs.android_base.platform.BaseApplication;

import timber.log.Timber;

public class BluetoothApplication extends BaseApplication<BluetoothTestComponent> {
    private BluetoothTestComponent bluetoothTestComponent;

    @Override
    public BluetoothTestComponent getAppComponent() {
        return bluetoothTestComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        setComponents();
    }

    private void setComponents() {
        bluetoothTestComponent = DaggerBluetoothTestComponent.builder()
                .bluetoothTestMainModule(new BluetoothTestMainModule(this))
                .build();
    }

}
