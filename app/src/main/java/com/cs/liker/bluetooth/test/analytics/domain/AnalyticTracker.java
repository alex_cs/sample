package com.cs.liker.bluetooth.test.analytics.domain;


public interface AnalyticTracker {

    void trackSupportPeripheral(boolean support);
}
