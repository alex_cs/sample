package com.cs.liker.bluetooth.test.analytics.platform;


import com.cs.liker.bluetooth.test.analytics.domain.AnalyticTracker;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AnalyticTrackerGoogleImpl implements AnalyticTracker {
    @Inject
    Tracker tracker;

    @Inject
    AnalyticTrackerGoogleImpl() {
    }

    @Override
    public void trackSupportPeripheral(boolean support) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction(support ? "Support peripheral" : "Does not support")
                .setValue(0)
                .build());
    }
}
