package com.cs.liker.bluetooth.test.ui.success;


import android.view.View;

import com.cs.android_base.platform.BasePresenterView;
import com.cs.liker.bluetooth.test.R;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.Subscriptions;

public class SuccessScreenView extends BasePresenterView {

    @Inject
    SuccessScreenView() {
        super(R.layout.view_success);
    }

    @Override
    public void injectViews(View view) {

    }

    @Override
    public Subscription bindPresenter() {
        return Subscriptions.empty();
    }
}
